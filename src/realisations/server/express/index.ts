import { ServerInterface } from "../../../abstractions/base/server"
import * as express from "express"
import * as cors from "cors"
import { Server } from "http"
const bodyParser = require("body-parser")

export abstract class ExpressServerAbstract implements ServerInterface {
  protected app: express.Application
  protected server?: Server
  constructor(app?: express.Application) {
    if (app) {
      this.app = app
    } else {
      this.app = express()
      this.app.use(cors())
      this.app.use(bodyParser.json())
    }
  }

  initialize = async (): Promise<void> => {
    await this.initListeners()
  }

  protected abstract initListeners: () => Promise<void>

  connect = async (port: number): Promise<void> => {
    this.server = this.app.listen(port)
  }

  disconnect = async (): Promise<void> => {
    if (this.server) await this.server.close()
  }
}

import { EndpointInterface } from "../../../abstractions/base/endpoint"
import { EndpointsAdapterInterface } from "../../../abstractions/base/endpoint/adapter"
import { RequestMethod } from "../../../abstractions/base/rest/adapter/category"
import { RestApiServerAdaperInterface } from "../../../abstractions/base/rest/adapter/server"
import { RestExpressServer } from "./rest"
import { ExpressServerAbstract } from "."

export class RestComplexServer extends ExpressServerAbstract {
  private endpointsAdapter: EndpointsAdapterInterface
  private restServer: RestExpressServer
  constructor(
    restAdapter: RestApiServerAdaperInterface,
    endpointsAdapter: EndpointsAdapterInterface,
  ) {
    super()
    this.restServer = new RestExpressServer(restAdapter, this.app)
    this.endpointsAdapter = endpointsAdapter
  }

  private initEndpointListener = (endpoint: EndpointInterface) => {
    if (endpoint.isMethodAllowed(RequestMethod.Post)) {
      this.app.post(endpoint.getName(), async (req, res) => {
        res.send(
          await this.endpointsAdapter.call(
            endpoint.getName(),
            RequestMethod.Post,
            req.body,
          ),
        )
      })
    }
  }

  private initEndpointsListeners = async (): Promise<void> => {
    const endpoints = this.endpointsAdapter.getEndpoints()
    for (let i = 0; i < endpoints.length; i++) {
      this.initEndpointListener(endpoints[i])
    }
  }

  protected initListeners = async (): Promise<void> => {
    await this.initEndpointsListeners()
    await this.restServer.initialize()
  }
}

enum ResponseStatus {
  Success = "ok",
  Error = "error",
}

type ResponseBase = {
  uuid: string
  status: ResponseStatus
}

type ErrorResponse = ResponseBase & {
  error: {
    message: string
  }
}

type SuccessResponse = ResponseBase & {
  [key: string]: any
}

export type Response = SuccessResponse | ErrorResponse

export abstract class ServerAdapterAbstract {
  // eslint-disable-next-line node/handle-callback-err
  protected getError = (error: Error, uuid: string): ErrorResponse => {
    return {
      status: ResponseStatus.Error,
      uuid,
      error: {
        message: error.message,
      },
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  protected getSuccess = (data: any, uuid: string): SuccessResponse => {
    try {
      data.status = ResponseStatus.Success
    } catch (e) {
      data = { status: ResponseStatus.Success }
    }
    data.uuid = uuid
    return data
  }
}

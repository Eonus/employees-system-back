import { ExpressServerAbstract } from "."
import { RequestMethod } from "../../../abstractions/base/rest/adapter/category"
import { RestApiServerAdaperInterface } from "../../../abstractions/base/rest/adapter/server"
import * as express from "express"

export class RestExpressServer extends ExpressServerAbstract {
  private adapter: RestApiServerAdaperInterface
  constructor(
    adapter: RestApiServerAdaperInterface,
    app?: express.Application,
  ) {
    super(app)
    this.adapter = adapter
  }

  protected initListeners = async (): Promise<void> => {
    this.app.get("/:category/:uuid", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RequestMethod.Get,
          req.query,
          req.params.uuid,
        ),
      )
    })
    this.app.get("/:category", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RequestMethod.GetAll,
          req.query,
        ),
      )
    })
    this.app.post("/:category", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RequestMethod.Post,
          req.body,
        ),
      )
    })
    this.app.put("/:category/:uuid", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RequestMethod.Update,
          req.body,
          req.params.uuid,
        ),
      )
    })
    this.app.delete("/:category/:uuid", async (req, res) => {
      res.send(
        await this.adapter.count(
          req.params.category,
          RequestMethod.Delete,
          req.body,
          req.params.uuid,
        ),
      )
    })
  }
}

import { EnvSchema, EnvType, load } from "ts-dotenv"
import { EnvInitializerInterface } from "../../abstractions/base/initializer/env"

export class EnvInitializer<Env>
  implements EnvInitializerInterface<EnvSchema, Env> {
  private env!: EnvType<EnvSchema>
  initialize = (schema: EnvSchema): void => {
    this.env = load(schema)
  }

  getEnv = (): Env => {
    return this.env as Env
  }
}

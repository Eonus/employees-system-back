import { RestApiCategoryAdapterAbstract } from "."
import { Filter } from "../../../../../abstractions/base/crud/filter"
import {
  ParseScheme,
  PrimitiveType,
} from "../../../../../abstractions/base/parser"
import {
  EmployeeInterface,
  Gender,
} from "../../../../../abstractions/employees"
import {
  EmployeeRestApiHandlerInterface,
  EmployeeRestObject,
} from "../../../../../abstractions/employees/handler"
import { parseData } from "../../../../parser"
import { getPartialParseScheme } from "../../../../parser/scheme"

export class RestApiEmployeeAdapter extends RestApiCategoryAdapterAbstract<
  EmployeeRestObject,
  EmployeeInterface,
  EmployeeRestApiHandlerInterface
> {
  protected scheme: ParseScheme = {
    primitives: [
      { name: "firstName", type: PrimitiveType.String },
      { name: "lastName", type: PrimitiveType.String },
      { name: "patronymic", type: PrimitiveType.String },
      {
        name: "gender",
        type: PrimitiveType.String,
        validate: (value: string): void => {
          const index = Object.values(Gender).indexOf(value as any)
          if (index === -1) throw Error("Parse status failed")
        },
      },
      { name: "contactInfo", type: PrimitiveType.String },
      { name: "salary", type: PrimitiveType.Number },
      { name: "position", type: PrimitiveType.String },
      { name: "isWorking", type: PrimitiveType.Boolean, canBeUndefined: true },
      { name: "childrenCount", type: PrimitiveType.Number },
      { name: "age", type: PrimitiveType.Number },
      { name: "experience", type: PrimitiveType.Number },
    ],
  }

  protected getAllScheme: ParseScheme = {
    primitives: [
      //regEx
      {
        name: "firstNameRegEx",
        type: PrimitiveType.String,
        canBeUndefined: true,
      },
      {
        name: "lastNameRegEx",
        type: PrimitiveType.String,
        canBeUndefined: true,
      },
      {
        name: "patronymicRegEx",
        type: PrimitiveType.String,
        canBeUndefined: true,
      },
      {
        name: "positionRegEx",
        type: PrimitiveType.String,
        canBeUndefined: true,
      },
      {
        name: "ageMore",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
      {
        name: "ageLess",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
      {
        name: "childrenCountMore",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
      {
        name: "childrenCountLess",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
      {
        name: "experienceMore",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
      {
        name: "experienceLess",
        type: PrimitiveType.Number,
        canBeUndefined: true,
      },
      {
        name: "showDismissed",
        type: PrimitiveType.Boolean,
        canBeUndefined: true,
      },
    ],
  }

  protected handler!: EmployeeRestApiHandlerInterface

  protected put = async (data: unknown, uuid?: string): Promise<any> => {
    if (!uuid) throw Error("No uuid sended")
    const product = parseData<Partial<EmployeeRestObject>>(
      getPartialParseScheme(this.scheme),
      data,
    )
    await this.handler.put(uuid, product)
  }

  protected getMany = async (data: unknown): Promise<any> => {
    const filterTemplate = parseData<{
      firstNameRegEx?: string
      lastNameRegEx?: string
      patronymicRegEx?: string
      positionRegEx?: string
      ageMore?: number
      ageLess?: number
      childrenCountMore?: number
      childrenCountLess?: number
      experienceLess?: number
      experienceMore?: number
      showDismissed?: boolean
    }>(this.getAllScheme, data)
    const filter: Filter<EmployeeInterface> = {}
    if (filterTemplate.firstNameRegEx) {
      filter.firstName = { $regex: filterTemplate.firstNameRegEx }
    }
    if (filterTemplate.lastNameRegEx) {
      filter.lastName = { $regex: filterTemplate.lastNameRegEx }
    }
    if (filterTemplate.patronymicRegEx) {
      filter.patronymic = { $regex: filterTemplate.patronymicRegEx }
    }
    if (filterTemplate.positionRegEx) {
      filter.position = { $regex: filterTemplate.positionRegEx }
    }
    const min = -10000
    const max = 100000
    filter.age = {
      $more: filterTemplate.ageMore || min,
      $less: filterTemplate.ageLess || max,
    }
    filter.childrenCount = {
      $more: filterTemplate.childrenCountMore || min,
      $less: filterTemplate.childrenCountLess || max,
    }
    filter.experience = {
      $more: filterTemplate.experienceMore || min,
      $less: filterTemplate.experienceLess || max,
    }
    filter.dismissed = {
      $value: filterTemplate.showDismissed || false,
    }
    return { elements: await this.handler.getMany(filter) }
  }
}

import { ParseScheme } from "../../../../../abstractions/base/parser"
import {
  RestApiCategoryAdapterInterface,
  RequestMethod,
} from "../../../../../abstractions/base/rest/adapter/category"
import {
  BaseRestObjectInterface,
  RestApiHandlerInterface,
} from "../../../../../abstractions/base/rest/handler"
import { parseData } from "../../../../parser"

export abstract class RestApiCategoryAdapterAbstract<
  RestObject extends BaseRestObjectInterface,
  FullObject extends RestObject,
  Handler extends RestApiHandlerInterface<RestObject, FullObject>
> implements RestApiCategoryAdapterInterface {
  protected handler: Handler
  protected abstract scheme: ParseScheme
  constructor(handler: Handler) {
    this.handler = handler
  }

  call = async (
    method: RequestMethod,
    data: unknown,
    uuid?: string,
  ): Promise<any> => {
    switch (method) {
      case RequestMethod.Get:
        return await this.getOne(data, uuid)
      case RequestMethod.GetAll:
        return await this.getMany(data)
      case RequestMethod.Post:
        return await this.post(data)
      case RequestMethod.Update:
        return await this.put(data, uuid)
      case RequestMethod.Delete:
        return await this.delete(data, uuid)
      default:
        throw Error("Such method is not available")
    }
  }

  protected getOne = async (_data: unknown, uuid?: string): Promise<any> => {
    if (!uuid) throw Error("No uuid sended")
    return { element: await this.handler.getOne(uuid) }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  protected getMany = async (_data?: unknown): Promise<any> => {
    return { elements: await this.handler.getMany() }
  }

  protected post = async (data: unknown): Promise<any> => {
    const object = <RestObject>parseData(this.scheme, data)
    return { objectUuid: await this.handler.post(object) }
  }

  protected delete = async (_data: unknown, uuid?: string): Promise<any> => {
    if (!uuid) throw Error("No uuid sended")
    await this.handler.delete(uuid)
  }

  protected abstract put: (data: any, uuid?: string) => Promise<any>
}

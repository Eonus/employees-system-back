import { RestApiCategoryAdapterInterface } from "../../../../abstractions/base/rest/adapter/category"
import { EmployeeRestApiHandlerInterface } from "../../../../abstractions/employees/handler"
import { RestApiEmployeeAdapter } from "./adapters/employee"

export class RestApiCategoryAdapterFactory {
  private employeeHandler: EmployeeRestApiHandlerInterface
  private employeeAdapter?: RestApiCategoryAdapterInterface
  constructor(employeeHandler: EmployeeRestApiHandlerInterface) {
    this.employeeHandler = employeeHandler
  }

  getAdapter = (category: string): RestApiCategoryAdapterInterface => {
    if (category === "employee") return this.getEmployeeAdapter()
    throw Error("Such category is not available")
  }

  getEmployeeAdapter = (): RestApiCategoryAdapterInterface => {
    if (!this.employeeAdapter) {
      this.employeeAdapter = new RestApiEmployeeAdapter(this.employeeHandler)
    }

    return this.employeeAdapter
  }
}

import { RequestMethod } from "../../../../abstractions/base/rest/adapter/category"
import { RestApiCategoryAdapterFactoryInterface } from "../../../../abstractions/base/rest/adapter/category/factory"
import { RestApiServerAdaperInterface } from "../../../../abstractions/base/rest/adapter/server"
import {
  ServerAdapterAbstract,
  Response,
} from "../../../server/express/adapter"

abstract class RestApiServerAdaperAbstract
  extends ServerAdapterAbstract
  implements RestApiServerAdaperInterface {
  abstract count: (
    category: string,
    method: RequestMethod,
    data: any
  ) => Promise<Response>

  abstract isCategoryExist: (category: string) => boolean
}

export class RestApiServerAdaper extends RestApiServerAdaperAbstract {
  private adapters: RestApiCategoryAdapterFactoryInterface
  constructor(adapters: RestApiCategoryAdapterFactoryInterface) {
    super()
    this.adapters = adapters
  }

  count = async (
    category: string,
    method: RequestMethod,
    data: unknown,
    uuid?: string,
  ): Promise<Response> => {
    const requestUuid = "request uuid"
    try {
      const adapter = this.adapters.getAdapter(category)
      return this.getSuccess(
        await adapter.call(method, data, uuid),
        requestUuid,
      )
    } catch (e) {
      return this.getError(e, requestUuid)
    }
  }

  isCategoryExist = (category: string): boolean => {
    try {
      this.adapters.getAdapter(category)
    } catch (e) {
      return false
    }
    return true
  }
}

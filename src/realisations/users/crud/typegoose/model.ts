import { getModelForClass, prop } from "@typegoose/typegoose"
import { UserInterface } from "../../../../abstractions/users"

class User implements UserInterface {
  @prop({ unique: true })
  login!: string

  @prop()
  password!: string

  @prop()
  email!: string

  @prop({ default: false })
  adminAccess?: boolean
}
export const UserModel = getModelForClass(User)

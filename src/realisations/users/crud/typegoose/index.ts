import { Filter } from "../../../../abstractions/base/crud/filter"
import { UserInterface } from "../../../../abstractions/users"
import { TypegooseStorageAbstract } from "../../../crud/typegoose"
import { UserModel } from "./model"

export class UserStorage extends TypegooseStorageAbstract<UserInterface> {
  create = async (object: UserInterface): Promise<void> => {
    try {
      await UserModel.create(object)
    } catch (e) {
      throw Error("Object with such uuid is already exist")
    }
  }

  read = async (login: string): Promise<UserInterface> => {
    const result = await UserModel.findOne({ login })
    if (!result) throw Error("Such was not found")
    return result
  }

  update = async (
    login: string,
    updateInfo: Partial<UserInterface>,
  ): Promise<void> => {
    const result = await UserModel.findOneAndUpdate({ login }, updateInfo)
    if (!result) throw Error("Such was not found")
  }

  delete = async (login: string): Promise<void> => {
    const result = await UserModel.findOneAndDelete({ login })
    if (!result) throw Error("Such was not found")
  }

  readMany = async (
    filter?: Filter<UserInterface>,
  ): Promise<UserInterface[]> => {
    return await UserModel.find(this.convertFilter(filter))
  }
}

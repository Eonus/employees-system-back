import {
  AuthBaseInterface,
  AuthProviderInterface,
} from "../../abstractions/base/auth"
import { StorageInterface } from "../../abstractions/base/crud"
import { UserInterface } from "../../abstractions/users"

export class AuthProvider implements AuthProviderInterface<UserInterface> {
  private storage: StorageInterface<UserInterface>
  constructor(storage: StorageInterface<UserInterface>) {
    this.storage = storage
  }

  register = async (info: UserInterface): Promise<void> => {
    await this.storage.create(info)
  }

  login = async (
    info: AuthBaseInterface,
  ): Promise<Omit<UserInterface, "password">> => {
    const user = await this.storage.read(info.login)
    if (info.password === user.password) {
      delete (user as any).password
      return user
    }
    throw Error("False password")
  }
}

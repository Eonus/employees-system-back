import { AuthProvider } from "."
import { AuthBaseInterface } from "../../abstractions/base/auth"
import { EndpointInterface } from "../../abstractions/base/endpoint"
import { ParseScheme, PrimitiveType } from "../../abstractions/base/parser"
import { RequestMethod } from "../../abstractions/base/rest/adapter/category"
import { UserInterface } from "../../abstractions/users"
import { parseData } from "../parser"

export class AuthEnpointsAdapter extends AuthProvider {
  private loginScheme: ParseScheme = {
    primitives: [
      {
        name: "login",
        type: PrimitiveType.String,
      },
      { name: "password", type: PrimitiveType.String },
    ],
  }

  private registerScheme: ParseScheme = {
    primitives: [
      {
        name: "login",
        type: PrimitiveType.String,
      },
      { name: "password", type: PrimitiveType.String },
      { name: "email", type: PrimitiveType.String },
    ],
  }

  safeLogin = async (data: unknown): Promise<any> => {
    const loginInfo = parseData<AuthBaseInterface>(this.loginScheme, data)
    return { user: await this.login(loginInfo) }
  }

  safeRegister = async (data: unknown): Promise<any> => {
    const registerInfo = parseData<UserInterface>(this.registerScheme, data)
    await this.register(registerInfo)
  }

  getEndpoints = (): EndpointInterface[] => {
    return [
      {
        count: this.safeLogin,
        getName: () => "/login",
        isMethodAllowed: (value: RequestMethod) => {
          if (value === RequestMethod.Post) return true
          return false
        },
      },
      {
        count: this.safeRegister,
        getName: () => "/register",
        isMethodAllowed: (value: RequestMethod) => {
          if (value === RequestMethod.Post) return true
          return false
        },
      },
    ]
  }
}

import { parseData } from "."
import { BaseStorageObjectInterface } from "../../abstractions/base/crud"
import { Filter, FilterValues } from "../../abstractions/base/crud/filter"
import {
  ComplexParseScheme,
  ParseScheme,
  PrimitiveType,
} from "../../abstractions/base/parser"
import { getPartialParseScheme } from "./scheme"

export const parseFilter = <ObjectInterface extends BaseStorageObjectInterface>(
  objectExample: ObjectInterface,
  data: unknown,
): Filter<ObjectInterface> => {
  let scheme: ParseScheme = { primitives: [], nested: [] }
  scheme = getFilterParseSchemeForObject(objectExample)
  scheme = getPartialParseScheme(scheme)
  const filter = parseData<Filter<ObjectInterface>>(scheme, data)
  return filter
}

export const getFilterParseSchemeForObject = (
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  object: any,
): ComplexParseScheme => {
  const scheme: ParseScheme = { primitives: [], nested: [] }
  for (const key in object) {
    if (typeof object[key] === "object") {
      const value = {
        ...getFilterParseSchemeForObject(object[key]),
        name: key,
      }
      scheme.nested?.push(value)
    } else {
      const value = {
        ...getPrimitiveParseScheme(
          typeof object[key],
          getStringEnumValues(FilterValues),
        ),
        name: key,
      }
      scheme.nested?.push(value)
    }
  }
  return scheme
}

const getPrimitiveParseScheme = (
  type: string,
  values: string[],
): ComplexParseScheme => {
  const scheme: ParseScheme = { primitives: [], nested: [] }
  for (let i = 0; i < values.length; i++) {
    scheme.primitives.push({
      name: values[i],
      type: getPrimitiveTypeByString(type),
      canBeUndefined: true,
    })
  }
  return scheme
}

const getPrimitiveTypeByString = (str: string): PrimitiveType => {
  switch (str) {
    case "string":
      return PrimitiveType.String
    case "number":
      return PrimitiveType.Number
    case "boolean":
      return PrimitiveType.Boolean
    case "any":
      return PrimitiveType.Any
    default:
      throw Error("Unknown primitive type")
  }
}

const getStringEnumValues = (object: { [key: string]: string }): string[] => {
  const keys = Object.values(object)
  return keys
}

import {
  Primitive,
  ParseScheme,
  ComplexParseScheme,
  PrimitiveType,
} from "../../abstractions/base/parser"

export const parseData = <T>(parseScheme: ParseScheme, data: unknown): T => {
  if ((<Primitive>parseScheme).type) {
    return getParam(parseScheme as Primitive, data) as T
  }
  parseScheme = parseScheme as ComplexParseScheme
  const result: any = {}
  for (let i = 0; i < parseScheme.primitives.length; i++) {
    const value = getParam(parseScheme.primitives[i], data)
    if (value !== undefined) result[parseScheme.primitives[i].name] = value
  }
  if (!parseScheme.nested) return result as T
  const dataObj = <{ [key: string]: any }>data
  for (let i = 0; i < parseScheme.nested?.length; i++) {
    const nestedName = parseScheme.nested[i].name
    if (!data) {
      throw Error("No " + nestedName + "(object) sended")
    }
    if (
      typeof dataObj[nestedName] === "undefined" ||
      dataObj[nestedName] === null
    ) {
      if (dataObj[nestedName] === null) {
        throwFalseTypeError(nestedName, "null", "object")
      }
      if (!parseScheme.nested[i].canBeUndefined) {
        throw Error("No " + nestedName + "(object) sended")
      }
    } else {
      const value = parseData(
        parseScheme.nested[i],
        (<{ [key: string]: any }>data)[nestedName],
      )
      result[parseScheme.nested[i].name] = value
    }
  }
  return result as T
}

const throwFalseTypeError = (
  field: string,
  type: string,
  neededType: string,
) => {
  throw Error(
    "False type with " + field + " - " + type + " instead " + neededType,
  )
}

const getBoolean = (str: string): boolean | undefined => {
  const isBoolean =
    // eslint-disable-next-line eqeqeq
    str == "true" || str == "1" || str == "false" || str == "0"
  if (!isBoolean) return undefined
  // eslint-disable-next-line eqeqeq
  return str == "true" || str == "1"
}

const tryToParse = (info: Primitive, data: any) => {
  if (typeof data[info.name] === "string") {
    if (info.type === PrimitiveType.Boolean) {
      const str: string = data[info.name]
      const result = getBoolean(str)
      if (result === undefined) {
        throwFalseTypeError(info.name, "string", "boolean")
      }
      data[info.name] = result
      return
    } else if (info.type === PrimitiveType.Number) {
      try {
        if (isNaN(data[info.name]) || !isFinite(data[info.name])) {
          throw Error("")
        }
        data[info.name] = parseInt(data[info.name])
      } catch (e) {
        throwFalseTypeError(info.name, "string", "number")
      }
      return
    }
  }
  throwFalseTypeError(info.name, typeof data[info.name], info.type)
}

const getParam = (info: Primitive, data: any): unknown => {
  if (!data) {
    throw Error("No " + info.name + "(" + info.type + ") sended")
  }
  if (typeof data[info.name] === "undefined" || data[info.name] === null) {
    if (typeof data[info.name] === "undefined") {
      if (info.canBeUndefined) return undefined
      else throw Error("No " + info.name + "(" + info.type + ") sended")
    }
    if (data[info.name] === null) {
      if (info.canBeNull) return null
      else {
        throwFalseTypeError(info.name, "null", info.type)
      }
    }
  }
  if (typeof data[info.name] !== info.type) {
    tryToParse(info, data)
  }
  if (info?.validate) info?.validate(data[info.name])
  return data[info.name]
}

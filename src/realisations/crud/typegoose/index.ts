import { StorageInterface } from "../../../abstractions/base/crud"
import { Filter, FilterValues } from "../../../abstractions/base/crud/filter"

export abstract class TypegooseStorageAbstract<ObjectInterface>
  implements StorageInterface<ObjectInterface> {
  abstract create: (object: ObjectInterface) => Promise<void>
  abstract read: (uuid: string) => Promise<ObjectInterface>
  abstract update: (
    uuid: string,
    updateInfo: Partial<ObjectInterface>
  ) => Promise<void>

  abstract delete: (uuid: string) => Promise<void>
  abstract readMany: (
    filter?: Filter<ObjectInterface>
  ) => Promise<ObjectInterface[]>

  public convertFilter = (filter?: Filter<ObjectInterface>): any => {
    if (!filter) return {}
    const result: any = {}
    for (const key in filter) {
      for (const filterValue in filter[key]) {
        switch (filterValue) {
          case FilterValues.More: {
            if (!result[key]) result[key] = { $gt: filter[key][filterValue] }
            else result[key].$gt = filter[key][filterValue]
            break
          }
          case FilterValues.Less: {
            if (!result[key]) result[key] = { $lt: filter[key][filterValue] }
            else result[key].$lt = filter[key][filterValue]
            break
          }
          case FilterValues.Regular: {
            if (!result[key]) result[key] = { $regex: filter[key][filterValue] }
            else result[key].$regex = filter[key][filterValue]
            break
          }
          case FilterValues.Value: {
            result[key] = filter[key][filterValue]
          }
        }
      }
    }

    return result
  }
}

import { EndpointInterface } from "../../abstractions/base/endpoint"
import { EndpointsAdapterInterface } from "../../abstractions/base/endpoint/adapter"
import { RequestMethod } from "../../abstractions/base/rest/adapter/category"
import { ServerAdapterAbstract } from "../server/express/adapter"

export class EndpointsAdapter
  extends ServerAdapterAbstract
  implements EndpointsAdapterInterface {
  private endpoints: Record<string, EndpointInterface> = {}
  constructor(endpoints: EndpointInterface[]) {
    super()
    this.setEndpoints(endpoints)
  }

  private setEndpoints = (endpoints: EndpointInterface[]) => {
    for (let i = 0; i < endpoints.length; i++) {
      this.endpoints[endpoints[i].getName()] = endpoints[i]
    }
  }

  getEndpoints = (): EndpointInterface[] => {
    const result = []
    for (const name in this.endpoints) result.push(this.endpoints[name])
    return result
  }

  call = async (
    endpointName: string,
    method: RequestMethod,
    data: unknown,
  ): Promise<any> => {
    if (!this.endpoints[endpointName]) {
      throw Error("Such endpoint is not available")
    }
    if (!this.endpoints[endpointName].isMethodAllowed(method)) {
      throw Error("Such method with this endpoint is not available")
    }
    const requestUuid = "request uuid"
    try {
      return this.getSuccess(
        await this.endpoints[endpointName].count(data),
        requestUuid,
      )
    } catch (e) {
      return this.getError(e, requestUuid)
    }
  }
}

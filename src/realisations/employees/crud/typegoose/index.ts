import { Filter } from "../../../../abstractions/base/crud/filter"
import { EmployeeInterface } from "../../../../abstractions/employees"
import { TypegooseStorageAbstract } from "../../../crud/typegoose"
import { EmployeeModel } from "./model"

export class EmployeeStorage extends TypegooseStorageAbstract<EmployeeInterface> {
  create = async (object: EmployeeInterface): Promise<void> => {
    try {
      await EmployeeModel.create(object)
    } catch (e) {
      throw Error("Object with such uuid is already exist")
    }
  }

  read = async (uuid: string): Promise<EmployeeInterface> => {
    const result = await EmployeeModel.findOne({ uuid })
    if (!result) throw Error("Such was not found")
    return result
  }

  update = async (
    uuid: string,
    updateInfo: Partial<EmployeeInterface>,
  ): Promise<void> => {
    const result = await EmployeeModel.findOneAndUpdate({ uuid }, updateInfo)
    if (!result) throw Error("Such was not found")
  }

  delete = async (uuid: string): Promise<void> => {
    const result = await EmployeeModel.findOneAndDelete({ uuid })
    if (!result) throw Error("Such was not found")
  }

  readMany = async (
    filter?: Filter<EmployeeInterface>,
  ): Promise<EmployeeInterface[]> => {
    return await EmployeeModel.find(this.convertFilter(filter))
  }
}

import { getModelForClass, prop } from "@typegoose/typegoose"
import { EmployeeInterface, Gender } from "../../../../abstractions/employees"

class Employee implements EmployeeInterface {
  @prop({ unique: true })
  uuid!: string

  @prop()
  firstName!: string

  @prop()
  lastName!: string

  @prop()
  patronymic!: string

  @prop()
  gender!: Gender

  @prop()
  contactInfo!: string

  @prop()
  creationTime!: number

  @prop()
  salary!: number

  @prop()
  position!: string

  @prop({ default: true })
  isWorking?: boolean

  @prop({ default: 0 })
  childrenCount!: number

  @prop({ default: 30 })
  age!: number

  @prop({ default: 5 })
  experience!: number

  @prop({ default: false })
  dismissed?: boolean
}
export const EmployeeModel = getModelForClass(Employee)

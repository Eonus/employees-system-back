import { StorageInterface } from "../../../abstractions/base/crud"
import { Filter } from "../../../abstractions/base/crud/filter"
import { EmployeeInterface } from "../../../abstractions/employees"
import { v4 as uuid } from "uuid"
import {
  EmployeeRestApiHandlerInterface,
  EmployeeRestObject,
} from "../../../abstractions/employees/handler"

export class EmployeeRestApiHandler implements EmployeeRestApiHandlerInterface {
  private storage: StorageInterface<EmployeeInterface>
  constructor(storage: StorageInterface<EmployeeInterface>) {
    this.storage = storage
  }

  getOne = async (uuid: string): Promise<EmployeeInterface> => {
    return await this.storage.read(uuid)
  }

  getMany = async (
    filter?: Filter<EmployeeInterface>,
  ): Promise<EmployeeInterface[]> => {
    return await this.storage.readMany(filter)
  }

  post = async (object: Omit<EmployeeRestObject, "uuid">): Promise<string> => {
    const fullObject: EmployeeInterface = {
      uuid: uuid(),
      ...object,
      creationTime: object.creationTime || Date.now(),
    }
    await this.storage.create(fullObject)
    return fullObject.uuid
  }

  put = async (
    uuid: string,
    object: Partial<EmployeeInterface>,
  ): Promise<void> => {
    await this.storage.update(uuid, object)
  }

  delete = async (uuid: string): Promise<void> => {
    await this.storage.update(uuid, { dismissed: true })
  }
}

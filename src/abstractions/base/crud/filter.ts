export enum FilterValues {
  More = "$more",
  Less = "$less",
  Regular = "$regex",
  Value = "$value",
}

export type OneKeyFilter<Value> = {
  [key in FilterValues]?: Value
}

export type Filter<ObjectInterface> = {
  [Key in keyof ObjectInterface]?: OneKeyFilter<ObjectInterface[Key]>
}

import { InitializerInterface } from "."

export interface MongooseInitializerInterface extends InitializerInterface {
  connect: (mongoUrl: string, dbName: string) => Promise<void>
  disconnect: () => Promise<void>
}

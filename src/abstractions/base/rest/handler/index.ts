import { Filter } from "../../crud/filter"

export interface BaseRestObjectInterface {
  uuid: string
}

export interface RestApiHandlerInterface<
  RestRequestObject extends BaseRestObjectInterface,
  RestResponseObject extends RestRequestObject
> {
  getOne: (uuid: string) => Promise<RestResponseObject>
  getMany: <T extends BaseRestObjectInterface>(
    filter?: Filter<T>
  ) => Promise<RestResponseObject[]>
  post: (object: Omit<RestRequestObject, "uuid">) => Promise<string>
  put: (uuid: string, object: Partial<RestRequestObject>) => Promise<void>
  delete: (uuid: string) => Promise<void>
}

export enum RequestMethod {
  Post,
  Get,
  Update,
  Delete,
  GetAll,
}

export interface RestApiCategoryAdapterInterface {
  call: (method: RequestMethod, data: any, uuid?: string) => Promise<any>
}

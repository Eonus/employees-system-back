import { RestApiCategoryAdapterInterface } from "."

export interface RestApiCategoryAdapterFactoryInterface {
  getAdapter: (category: string) => RestApiCategoryAdapterInterface
}

import { RequestMethod } from "../category"

export interface RestApiServerAdaperInterface {
  count: (
    category: string,
    method: RequestMethod,
    data: any,
    uuid?: string
  ) => Promise<any>
  isCategoryExist: (category: string) => boolean
}

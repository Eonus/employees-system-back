import { UserInterface } from "../../users"

export interface AuthBaseInterface {
  login: string
  password: string
}

export interface AuthProviderInterface<
  AuthInterface extends AuthBaseInterface
> {
  register: (info: AuthInterface) => Promise<void>
  login: (info: AuthBaseInterface) => Promise<Omit<UserInterface, "password">>
}

export interface ConverterInterface<From, To> {
  convert: (from: From) => To
}

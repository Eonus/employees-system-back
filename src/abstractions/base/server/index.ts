export interface ServerInterface {
  initialize: () => Promise<void>
  connect: (port: number) => Promise<void>
  disconnect: () => Promise<void>
}

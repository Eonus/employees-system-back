import { RequestMethod } from "../rest/adapter/category"

export interface EndpointInterface {
  count: (data: any) => Promise<any>
  getName: () => string
  isMethodAllowed: (method: RequestMethod) => boolean
}

import { EndpointInterface } from "."
import { RequestMethod } from "../rest/adapter/category"

export interface EndpointsAdapterInterface {
  call: (endpointName: string, method: RequestMethod, data: any) => Promise<any>
  getEndpoints: () => EndpointInterface[]
}

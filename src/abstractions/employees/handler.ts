import { EmployeeInterface, Gender } from "."
import {
  BaseRestObjectInterface,
  RestApiHandlerInterface,
} from "../base/rest/handler"

export interface EmployeeRestObject
  extends BaseRestObjectInterface,
    Partial<EmployeeInterface> {
  uuid: string
  firstName: string
  lastName: string
  patronymic: string
  gender: Gender
  contactInfo: string
  salary: number
  position: string
  isWorking?: boolean
  childrenCount: number
  age: number
  experience: number
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface EmployeeRestApiHandlerInterface
  extends RestApiHandlerInterface<EmployeeRestObject, EmployeeInterface> {}

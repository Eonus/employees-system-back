export enum Gender {
  Male = "male",
  Female = "female",
}

export interface EmployeeInterface {
  uuid: string
  firstName: string
  lastName: string
  patronymic: string
  gender: Gender
  contactInfo: string
  creationTime: number
  salary: number
  position: string
  isWorking?: boolean
  childrenCount: number
  age: number
  experience: number
  dismissed?: boolean
}

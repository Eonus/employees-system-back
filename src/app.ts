import { EmployeeStorage } from "./realisations/employees/crud/typegoose"
import { EmployeeRestApiHandler } from "./realisations/employees/handler"
import { EnvInitializer } from "./realisations/initializers/env"
import { MongooseInitializer } from "./realisations/initializers/mongoose"
import { RestApiCategoryAdapterFactory } from "./realisations/rest/adapter/category/factory"
import { RestApiServerAdaper } from "./realisations/rest/adapter/server"
import { RestComplexServer } from "./realisations/server/express/complex"
import { EndpointsAdapter } from "./realisations/endpoints/adapter"
import { UserStorage } from "./realisations/users/crud/typegoose"
import { AuthEnpointsAdapter } from "./realisations/auth/adapter"

const envInitializer = new EnvInitializer<{
  MONGODB_STRING: string
  DB_NAME: string
  PORT: number
}>()
envInitializer.initialize({
  MONGODB_STRING: String,
  DB_NAME: String,
  PORT: Number,
})
const env = envInitializer.getEnv()
const mongooseInitializer = new MongooseInitializer()
mongooseInitializer.connect(env.MONGODB_STRING, env.DB_NAME)

const employeeStorage = new EmployeeStorage()
const employeeHandler = new EmployeeRestApiHandler(employeeStorage)

const userStorage = new UserStorage()
const authAdapter = new AuthEnpointsAdapter(userStorage)

const server = new RestComplexServer(
  new RestApiServerAdaper(new RestApiCategoryAdapterFactory(employeeHandler)),
  new EndpointsAdapter(authAdapter.getEndpoints()),
)
server.initialize()
server.connect(env.PORT)
